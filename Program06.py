#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 13:15:31 2023

@author: putta
"""

def gcd(a, b):
    """
    Returns the Greatest Common Divisor (GCD) of two integers using Euclid's Algorithm.
    """
    while b != 0:
        a, b = b, a % b
    return a

m = int(input("Enter first number : "))
n = int(input("Enter second number : "))

res = gcd(m,n)

print("GCD of", m, "and", n, "is", res)