#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 13:49:11 2023

@author: putta
"""

import numpy as np
import matplotlib.pyplot as plt

"""
Lets solve
2x + 3y = 8
4x + 9y = 15
"""

A = np.array([[2, 3], [4, 9]])
b = np.array([8, 15])

x = np.linalg.solve(A, b)

if np.linalg.det(A) != 0:
    print("The system of equations is consistent.")
else:
    print("The system of equations is inconsistent.")

# Plot the two equations as lines
x_vals = np.linspace(-8, 8, 100)
y1_vals = (8 - 2*x_vals)/3
y2_vals = (15 - 4*x_vals)/9
plt.plot(x_vals, y1_vals, label="2x + 3y = 8")
plt.plot(x_vals, y2_vals, label="4x + 9y = 15")

# Plot the intersection point as a red dot
plt.plot(x[0], x[1], 'ro', label="Intersection")

#show grid
plt.grid(True)
plt.legend()
plt.xlabel("x")
plt.ylabel("y")
plt.title("System of Linear Equations")
plt.show()
