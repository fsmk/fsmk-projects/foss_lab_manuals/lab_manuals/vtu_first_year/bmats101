#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 13:57:53 2023

@author: putta
"""

import numpy as np

def rayleigh_power(A, y, max_iter=100, tol=1e-6):
    """Compute the largest eigenvalue and eigenvector of a matrix A using the Rayleigh power method."""
    iter = 0
    lam = 0
    y = y / np.linalg.norm(y)  # normalize y
    while iter < max_iter:
        y_old = y
        lam_old = lam
        y = np.dot(A, y)
        lam = np.dot(y, y_old)
        y = y / np.linalg.norm(y)  # normalize y
        if np.abs(lam - lam_old) < tol:
            break
        iter += 1
    return lam, y, iter

def inverse_rayleigh_power(A, y, max_iter=100, tol=1e-6):
    """Compute the smallest eigenvalue and eigenvector of a matrix A using the inverse Rayleigh power method."""
    iter = 0
    lam = 0
    y = y / np.linalg.norm(y)  # normalize y
    while iter < max_iter:
        y_old = y
        lam_old = lam
        y = np.linalg.solve(A, y)
        lam = np.dot(y, y_old)
        y = y / np.linalg.norm(y)  # normalize y
        if np.abs(lam - lam_old) < tol:
            break
        iter += 1
    lam = 1/lam
    return lam, y, iter

# Define the matrix A and the initial guess for the eigenvector
A = np.array([[3, -1, 1], [-1, 3, 1], [1, 1, 5]])
y = np.array([1, 1, 1])

# Use the Rayleigh power method to find the largest eigenvalue and eigenvector
lam1, v, iter1 = rayleigh_power(A, y)
print("Largest eigenvalue of A:", lam1)
print()

# Use the inverse Rayleigh power method to find the smallest eigenvalue and eigenvector
lam2, y1, iter2 = inverse_rayleigh_power(A, y)
print("Smallest eigenvalue of A:", lam2)
print()

# Print the eigenvectors corresponding to the eigenvalues
print("Eigenvectors of A:")
print(v)
print()

# Print the number of iterations required for each method
print("Number of iterations for Rayleigh power method:", iter1)
print("Number of iterations for inverse Rayleigh power method:", iter2)
